const { PubSub } = require('apollo-server');
const queryResolver = require('./query');
const mutationResolver = require('./mutation');
const bookResolver = require('./book');

const pubsub = new PubSub();

module.exports = {
  Query: queryResolver,
  Mutation: mutationResolver,
  Book: bookResolver,
};
